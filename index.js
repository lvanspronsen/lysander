var AuditRepository = require('./src/audit-repository.js');
var auth = require('./src/auth.js');
var context = require('./src/context.js');
var CorsProvider = require('./src/cors.js');
var errors = require('./src/errors.js');
var Eventer = require('./src/eventer.js');
var identity = require('./src/identity.js');
var Query = require('./src/query.js');
var RepositoryHoster = require('./src/repo-hoster.js');
var Repository = require('./src/repository.js');
var SoftAuditRepository = require('./src/soft-audit-repository.js');
var TableInfo = require('./src/table-info.js');

module.exports = {

  // auth utility functions
  auth : {
    genSalt : auth.genSalt,
    hash : auth.hash,
    compare : auth.compare
  },

  // errors
  NotFoundError : errors.NotFoundError,
  AccessDeniedError : errors.AccessDeniedError,
  InvalidCredentialsError : errors.InvalidCredentialsError,

  // main classes
  AuditRepository : AuditRepository,
  AuthProvider : auth.AuthProvider,
  Context : context.Context,
  ContextProvider : context.ContextProvider,
  CorsProvider : CorsProvider,
  ErrorHandler : errors.ErrorHandler,
  Eventer : Eventer,
  Identity : identity.Identity,
  IdentityProvider : identity.IdentityProvider,
  Query : Query,
  RepositoryHoster : RepositoryHoster,
  Repository : Repository,
  SoftAuditRepository : SoftAuditRepository,
  TableInfo : TableInfo
};
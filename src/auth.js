var _ = require('underscore');
var q = require('q');
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var moment = require('moment');
var errors = require('./errors.js');

/* promise interface to bcrypt.genSalt */

var genSalt = function() {
  var deferred = q.defer();
  bcrypt.genSalt(10, function(err, salt) {
    if(err) {
      deferred.reject(err);
    } else {
      deferred.resolve(salt);
    }
  });
  return deferred.promise;
};

/* promise interface to bcrypt.hash */

var hash = function(value, salt) {
  var deferred = q.defer();
  bcrypt.hash(value, salt, null, function(err, hash) {
    if(err) {
      deferred.reject(err);
    } else {
      deferred.resolve(hash);
    }
  });
  return deferred.promise;
};

/* promise interface to bcrypt.genSalt and bcrypt.hash */

var saltAndHash = function(value) {
  return genSalt()
    .then(function(salt) {
      return hash(value, salt);
    });
};

/* promise interface to bcrypt.compare */

var compare = function(value, hash) {
  var deferred = q.defer();
  bcrypt.compare(value, hash, function(err, result) {
    if(err) {
      deferred.reject(err);
    } else {
      deferred.resolve(result);
    }
  });
  return deferred.promise;
};

/* promise interface to create a random hex token */

var randomToken = function(length) {
  return crypto.pseudoRandomBytes(length / 2).toString('hex');
};

/* Auth Provider implementation */

var AuthProvider = function(usersRepo, tokensRepo, contextProvider, identityProvider, errorHandler) {
  this.usersRepo = usersRepo;
  this.tokensRepo = tokensRepo;
  this.contextProvider = contextProvider;
  this.identityProvider = identityProvider;
  this.errorHandler = errorHandler;

  /* Make sure we hash user passwords whenever they are inserted */
  usersRepo.on('inserting', function(context, obj) {

    // a system context can change the password to whatever it pleases
    if(context.identity.isSystem() && obj.password && typeof obj.password === 'string') {
      return saltAndHash(obj.password)
        .then(function(hash) {
          obj.password = hash;
        });
    }
    // a user context must provide an object containing the old password
    else if(context.identity.isUser() && obj.password && typeof obj.password === 'object') {
      var oldPassword = obj.password.oldPassword;
      var newPassword = obj.password.newPassword;


      return this.getUserById(context.identity.user.id)
        .then(function(user) { return compare(oldPassword, user.password); })
        .then(function(result) {
          if(result) {
            return saltAndHash(newPassword)
              .then(function(hash) {
                obj.password = hash;
              });
          }
        });
    }

    return null;
  });

  /* Make sure we hash user passwords whenever they are updated */
  usersRepo.on('updating', function(context, old, obj) {

    // a system context can change the password to whatever it pleases
    if(context.identity.isSystem() && obj.password && typeof obj.password === 'string') {
      return saltAndHash(obj.password)
        .then(function(hash) {
          obj.password = hash;
        });
    }
    // a user context must provide an object containing the old password
    else if(context.identity.isUser() && obj.password && typeof obj.password === 'object') {
      var oldPassword = obj.password.oldPassword;
      var newPassword = obj.password.newPassword;


      return this.getUserById(context.identity.user.id)
        .then(function(user) { return compare(oldPassword, user.password); })
        .then(function(result) {
          if(result) {
            return saltAndHash(newPassword)
              .then(function(hash) {
                obj.password = hash;
              });
          }
        });
    }

    return null;
  });
};

AuthProvider.prototype.handleError = function(res, err) {
  this.errorHandler.handle(res, err);
};

// just creates a new object representing an auth token
AuthProvider.prototype.newToken = function(user, device) {
  return {
    id : randomToken(30),
    user_id : user.id,
    device : device,
    issued_at : new Date(),
    expires_at : moment().add('months', 1).toDate()
  };
};

// retrieves a user by its ID
AuthProvider.prototype.getUserById = function(id) {
  var context = this.contextProvider.system();
  return this.usersRepo.find(context, id);
};

// retrieves a user by its email address
AuthProvider.prototype.getUserByEmail = function(email) {
  var context = this.contextProvider.system();
  return this.usersRepo.query(context)
    .where('email', '=', email)
    .first(false)
    .then(function(user) {
      if(user == null) {
        throw new errors.InvalidCredentialsError('invalid credentials');
      }
      return user;
    });
};

// creates an auth token for the supplied user and inserts
// it into the database
AuthProvider.prototype.createToken = function(user, device) {

  var me = this;
  var token = this.newToken(user, device);
  var context = me.contextProvider.system();
  return me.tokensRepo.insert(context, token)
    .then(function() { return token; });
};

// authenticates a user and generates an auth token
// if authentication succeeds
AuthProvider.prototype.authenticate = function(email, password, device) {
  var me = this;
  var user = null;

  return this.getUserByEmail(email)
    .then(function(u) { user = u; })
    .then(function() { return compare(password, user.password); })
    .then(function(result) {
      if(result) {
        return me.createToken(user, device);
      } else {
        throw new errors.InvalidCredentialsError('invalid credentials');
      }
    });
};

// retrieves an existing auth token from the database
AuthProvider.prototype.getToken = function(id) {
  var me = this;

  var context = this.contextProvider.system();
  return this.tokensRepo.find(context, id);
};

AuthProvider.prototype.getValidToken = function(id) {
  var me = this;

  return me.getToken(id)
    .then(function(token) {
      if(token.expires_at > (new Date())) {
        return token;
      } else {
        throw new errors.NotFoundError('token is expired');
      }
    });
};


// revokes an existing auth token
AuthProvider.prototype.revokeToken = function(id) {
  var me = this;
  var context = this.contextProvider.system();
  return me.tokensRepo.update(context, {
    id : id,
    expires_at : new Date()
  });
};

// hosts the auth provider service in an express application

AuthProvider.prototype.host = function(app, prefix) {

  var me = this;

  // Auth middleware, which determines if the current request
  // is authenticated, and sets the user of the current context
  // if it is
  app.use(function(req, res, next) {

    // check to see if a context object exists
    var context = req.context;
    if(!context) {
      next();
      return;
    }

    // check to see if a token has been supplied,
    // if not, then we are not authenticated
    if(!req.query || !req.query.token) {
      next();
      return;
    }

    // retrieve the token and, if it is still valid,
    // retrieve the user that the request is operating under
    var id = req.query.token;
    var token = null;
    
    me.getValidToken(id)
      .then(function(token) {
        return me.getUserById(token.user_id);
      })
      .then(function(user) {
        var identity = me.identityProvider.fromUser(user);
        context.setIdentity(identity);
        next();
      }, function(err) {
        // if we have problems authenticating the user,
        // we simply proceed to execute the request
        // without assigning a user to the context, and assume
        // that other operations will fail with the appropriate
        // security checks
        next();
      });
  });

  // POST:authenticate - checks to see if the provided credentials
  // are valid. If they are, inserts a new auth token and returns
  // it as a JSON result
  app.post(prefix + 'authenticate', function(req, res) {

    if(!req.body || !req.body.email || !req.body.password) {
      res.send({ error : true, message : 'No credentials provided' }, 400);
      return;
    }

    var email = req.body.email, password = req.body.password, device = req.body.device;
    me.authenticate(email, password, device)
      .then(function(token) {
        res.send(token, 200);
      }, me.handleError.bind(me,res));
  });

  // POST:restore - checks to see if an auth token is still valid.
  // If it is, retrieves the auth token from the database and
  // returns it as a JSON result
  app.post(prefix + 'restore', function(req, res) {
    if(!req.body || !req.body.id) {
      res.send({ error : true, message : 'No token id provided' }, 400);
      return;
    }

    var id = req.body.id;
    me.getToken(id)
      .then(function(token) {
        if(token.expires_at > (new Date())) {
          res.send(token, 200);
        } else {
          res.send({ error : true, message : 'Token has expired' }, 500);
        }
      }, me.handleError.bind(me,res));     
  });

  // POST:revoke - forces an existing auth token to become expired
  app.post(prefix + 'revoke', function(req, res) {
    if(!req || !req.body.id) {
      res.send({ error : true, message : 'No token id provided' }, 400);
      return;
    }

    var id = req.body.id;
    me.revokeToken(id)
      .then(function() {
        res.send({}, 200);
      }, me.handleError.bind(me,res));
  });

};

// module exports

module.exports = {
  genSalt : genSalt,
  hash : hash,
  compare : compare,
  AuthProvider : AuthProvider
};

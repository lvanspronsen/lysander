var _ = require('underscore');
var q = require('q');


// Base type for identity
var Identity = function(type, val) {
  this.type = type;
  if(val) {
    this[type] = val;
  }
};

// Checks whether the identity is a user identity
Identity.prototype.isUser = function() {
  return this.type == 'user';
};

// Checks whether the identity is a system (root) identity
Identity.prototype.isSystem = function() {
  return this.type == 'system';
};


var IdentityProvider = function() {
};

// creates an identity object from
// a user
IdentityProvider.prototype.fromUser = function(user) {
  return new Identity('user', user);
};

// constructs a new system identity
IdentityProvider.prototype.system = function() {
  return new Identity('system');
};

// export both constructors

module.exports = {
  Identity : Identity,
  IdentityProvider : IdentityProvider
};
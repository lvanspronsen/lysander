var _ = require('underscore');
var q = require('q');
var errors = require('./errors.js');
var Eventer = require('./eventer.js');
var Query = require('./query.js').Query;
var TableInfo = require('./table-info.js');

/* Repository extends the Eventer object, and exposes
 * these events:
 *   - finding
 *   - found
 *   - querying
 *   - queried
 *   - inserting
 *   - inserted
 *   - updating
 *   - updated
 *   - saving (updating or inserting)
 *   - saved  (updated or inserted)
 *   - deleting
 *   - deleted
 */

function Repository(db) {
  Eventer.apply(this, arguments);
  this.db = db;
  this.table = '';
  this.views = [];
  this.schema = { columns : [], views : {} };
};
Repository.prototype = Object.create(Eventer.prototype);

/* Applies the default filter to a query to retrieve data,
 * this is usually used to limit retrieval to records that a user
 * has permission to access */
Repository.prototype.defaultFilter = function(context, query, view) {
};

/* Applies filters from a query object to the supplied
 * database query */
Repository.prototype.applyFilters = function(query, queryableFields, filters) {

  var applyFilter = function(query, conjunctive, field, op, value) {

    if((op === '=' || op === '!=') && value === null) {
      var fn;
      if(conjunctive && op === '=') {
        fn = 'whereNull';
      } else if(conjunctive && op === '!=') {
        fn = 'whereNotNull';
      } else if(op === '=') {
        fn = 'orWhereNull';
      } else {
        fn = 'orWhereNotNull';
      }

      return query[fn](field);
    } else if(op == 'IN') {
      var fn;
      if(conjunctive) {
        fn = 'whereIn';
      } else {
        fn = 'orWhereIn';
      }

      return query[fn](field, value);
    } else if(conjunctive) {
      return query.where(field, op, value);
    } else {
      return query.orWhere(field, op, value);
    }
  };

  var groupFilters = function(_filters, _group) {

    var conjunctive = _group == 'all';
    return function() {
      var _query = this;
      _filters.forEach(function(f, i) {
        if (f.group && conjunctive) {
          if(f.filters.length === 0) {
            return _query;
          }
          return _query.where(groupFilters(f.filters, f.group));
        } else if(f.group && !conjunctive) {
          return _query.orWhere(groupFilters(f.filters, f.group));
        } else {
          return applyFilter(_query, conjunctive, f.field, f.op, f.value);
        }
      });
    };
  };

  // remove all filters that aren't part of queryable fields
  var removeUnqueryable = function(filters) {
    for(var i = 0; i < filters.length; i++) {
      var f = filters[i];
      if(f.group) {
        removeUnqueryable(f.filters);
      } else if (queryableFields.indexOf(f.field) == -1) {
        filters.splice(i,1);
        i--;
      }
    }
  }

  if(filters.length !== 0) {
    query.where(groupFilters(filters, 'all'));
  }
};

/* Applies ordering from a query object to the supplied
 * database query */
Repository.prototype.applyOrders = function(query, queryableFields, orders) {
  for(var i = 0; i < orders.length; i++) {
    var o = orders[i];
    if(queryableFields.indexOf(o.field) == -1) {
      continue;
    }

    query.orderBy(o.field, o.dir == 'asc' ? 'ASC' : 'DESC');
  }
};

/* Limits the query to a certain subset of fields */
Repository.prototype.applyFields = function(query, queryableFields, fields) {
  fields = _.intersection(queryableFields, fields);
  if(fields && fields.length > 0) {
    query.select(fields);
  } else {
    query.select(queryableFields);
  }
};

/* Applies skip and take parameters */
Repository.prototype.applySkipAndTake = function(query, skip, take) {
  if(skip) {
    query.offset(skip);
  }

  if(take) {
    query.limit(take);
  }
};

/* Not an actual permission checks. Just verifies that there
 * is indeed an identity bound to the provided context */
Repository.prototype.verifyIdentity = function(context) {
  var identity = context.getIdentity();
  if(!identity) {
    throw new errors.AccessDeniedError('access denied');
  }
};

/* Retrieves the main table that records get inserted into or updated in */
Repository.prototype.getMainTable = function() {
  return this.table;
};

/* Retrieves the fields that are queryable */
Repository.prototype.getQueryableFields = function(context, view) {
  var fields = null;
  if(!view || view == this.getMainTable()) {
    fields = _.pluck(this.schema.columns, 'name');
  } else {
    fields = _.pluck(this.schema.views[view], 'name');
  }
  return fields;
};

/* Retrieves the fields that are updateable */
Repository.prototype.getUpdateableFields = function(context) {
  var names = _.pluck(this.schema.columns, 'name');
  return _.without(names, 'id');
};

/* Retrieves the fields that are insertable */
Repository.prototype.getInsertableFields = function(context) {
  var names = _.pluck(this.schema.columns, 'name');
  return _.without(names, 'id');
};

/* Retrieves the schema information about the repository */
Repository.prototype.getSchema = function(context) {
  var me = this;

  return q(null)
    .then(this.verifyIdentity.bind(this,context))
    .then(function() {
      var schema = {
        insertable : me.getInsertableFields(context),
        updateable : me.getUpdateableFields(context),
        queryable : me.getQueryableFields(context),
        views : {}
      };

      _.each(me.views, function(view) {
        var columns = me.getQueryableFields(context, view);
        schema.views[view] = columns;
      });

      return schema;
    });
};

/* Initializes the repository */
Repository.prototype.init = function() {
  var me = this;
  var proms = [];

  /* load columns for the main table */
  var table = this.getMainTable();
  var tableInfo = new TableInfo(me.db, table);
  var prom = tableInfo.getColumns()
    .then(function(columns) {
      me.schema.columns = columns;
    });
  proms.push(prom);

  /* load columns for the views */
  _.each(this.views, function(view) {
    var viewInfo = new TableInfo(me.db, view);
    var vprom = viewInfo.getColumns()
      .then(function(columns) {
        me.schema.views[view] = columns;
      });
    proms.push(vprom);
  });

  return q.all(proms);
};

/* Finds an object by its id */
Repository.prototype.doFind = function(context, id, view) {
  if(!view) {
    view = this.table;
  }

  var query = this.db(view);
  this.defaultFilter(context,query,view);
  query.where('id', '=', id);

  return query.then(function(results) {
    if(results.length > 0) {
      return results[0];
    } else {
      throw new errors.NotFoundError('could not find object');
    }
  });
};

/* Finds an object by its ID value */
Repository.prototype.find = function(context, id,view) {  

  var me = this;
  var obj = null;

  return q(null)
    .then(me.verifyIdentity.bind(me,context))
    .then(function() { return me.emit('finding', context, id); })
    .then(function() { return me.doFind(context, id,view); })
    .then(function(result) { obj = result; })
    .then(function() { return me.emit('found', context, obj); })
    .then(function() { return obj; });
};

/* Finds the first object matching query conditions */
Repository.prototype.doFirst = function(context, require, query) {
  if(query) {
    query.take = 1;
  } else {
    query = { take : 1 };
  }

  return this.doQuery(context, query)
    .then(function(results) {
      if(results.length > 0) {
        return results[0];
      } else if(!require) {
        return null;
      } else {
        throw new errors.NotFoundError('could not find object');
      }
    });
};

/* Finds the first object matching query conditions */
Repository.prototype.first = function(context, require, query) {

  /* For server side first queries, use:
   *    repo.query(context).<blah>.first(require);
   */

  var me = this;
  var obj = null;
  var result = null;

  return q(null)
    .then(me.verifyIdentity.bind(me,context))
    .then(function() { return me.emit('querying', context, query); })
    .then(function() { return me.doFirst(context, require, query); })
    .then(function(r) { result = r; })
    .then(function() { return me.emit('queried', context, query, [result]); })
    .then(function() { return result; });
};

/* Performs an actual query on the database table or view */
Repository.prototype.doQuery = function(context, query) {
  var view = this.table;
  if(query.view && this.views.indexOf(query.view) !== -1) {
    view = query.view;
  }

  var q = this.db(view);
  if(context.transaction) {
    q.transacting(context.transaction);
  }

  /* apply additional clauses */
  this.defaultFilter(context, q, view);

  var queryableFields = this.getQueryableFields(context, view);
  this.applyFilters(q, queryableFields, query.filters || []);
  this.applyOrders(q, queryableFields, query.orderBy || []);
  this.applySkipAndTake(q, query.skip, query.take);

  if(query.count) {
    return q.count('*').then(function(result) {
      return { count : parseInt(result[0].aggregate) };
    })
  } else {
    this.applyFields(q, queryableFields, query.fields);
  }

  return q;
};

/* Queries the database table/view for specific rows */
Repository.prototype.query = function(context, query) {

  if(!query) {
    // this is a request for server-side query construction
    return new Query(context, this);
  }

  var me = this;
  var results = null;

  return q(null)
    .then(me.verifyIdentity.bind(me,context))
    .then(function() { return me.emit('querying', context, query); })
    .then(function() { return me.doQuery(context, query); })
    .then(function(r) { results = r; })
    .then(function() { return me.emit('queried', context, query, results); })
    .then(function() { return results; });
};

/* Performs an actual database insertion */
Repository.prototype.doInsert = function(context, obj) {
  var fields = this.getInsertableFields(context);
  obj = _.pick(obj, fields);

  var q = this.db(this.table);
  if(context.transaction) {
    q.transacting(context.transaction);
  }

  return q.insert(obj, 'id')
    .then(function(newIds) { return newIds[0]; });
};

/* Performs an insertion of an object into the database */
Repository.prototype.insert = function(context, obj) {

  var me = this;
  var id = null;

  return q(null)
    .then(me.verifyIdentity.bind(me,context))
    .then(function() { return me.emit('inserting', context, obj); })
    .then(function() { return me.emit('saving', context, obj); })
    .then(function() { return me.doInsert(context, obj); })
    .then(function(newId) { id = newId; })
    .then(function() { return me.doFind(context, id) })
    .then(function(newObj) { obj = newObj; })
    .then(function() { return me.emit('saved', context, obj); })
    .then(function() { return me.emit('inserted', context, obj); })
    .then(function() { return obj; });
};

/* Performs an actual database update */
Repository.prototype.doUpdate = function(context, old, update) {
  var fields = this.getUpdateableFields(context);
  update = _.pick(update, fields);

  var id = old.id;
  var q = this.db(this.table);
  if(context.transaction) {
    q.transacting(context.transaction);
  }
  return q.where('id', '=', id).update(update);
}

/* Performs an update of an object in the database */
Repository.prototype.update = function(context, obj) {

  var me = this;
  var id = obj.id;
  var old = null;

  return q(null)
    .then(me.verifyIdentity.bind(me,context))
    .then(me.doFind.bind(me,context,id))
    .then(function(obj) { old = obj; })
    .then(function() { return me.emit('updating', context, old, obj); })
    .then(function() { return me.emit('saving', context, old, obj); })
    .then(function() { return me.doUpdate(context, old, obj); })
    .then(function() { return me.emit('saved', context, old, obj); })
    .then(function() { return me.emit('updated', context, old, obj); });
};

/* Performs the actual deletion of an object */
Repository.prototype.doDelete = function(context, id) {
  var q = this.db(this.table);
  if(context.transaction) {
    q.transacting(context.transaction);
  }
  return q.where('id', '=', id).del();
};

/* Performs a deletion of an object from the database */
Repository.prototype.del = function(context, id) {
  var me = this;
  var old = null;

  return q(null)
    .then(me.verifyIdentity.bind(me,context))
    .then(me.doFind.bind(me, context, id))
    .then(function(obj) { old = obj; })
    .then(function() { return me.emit('deleting', context, old); })
    .then(function() { return me.doDelete(context, id); })
    .then(function() { return me.emit('deleted', context, old); });
};

/* Applies a changeset to the repository, which can insert, update or delete
 * multiple objects */
Repository.prototype.changeset = function(context, changeset) {
  var insert = changeset.insert;
  var update = changeset.update;
  var del = changeset.del;
  var prom = q(null);

  if(insert) {
    for(var i = 0; i < insert.length; i++) {
      var obj = insert[i];
      prom = prom.then(this.insert.bind(this, context, obj));
    }
  }

  if(update) {
    for(var i = 0; i < update.length; i++) {
      var obj = update[i];
      prom = prom.then(this.update.bind(this, context, obj));
    }
  }

  if(del) {
    for(var i = 0; i < del.length; i++) {
      var id = del[i];
      prom = prom.then(this.del.bind(this, context, id));
    }
  }

  return prom;
};

module.exports = Repository;

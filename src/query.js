var _ = require('underscore');
var q = require('q');

var Query = function(context, repo) {
  this.context = context;
  this.repo = repo;
  this.query = {};
};

/* sets the view that is being queried */
Query.prototype.view = function(view) {
  this.query.view = view;
  return this;
};

/* Fields query result on a fields value */
Query.prototype.where = function(field, op, value) {
  if(!value) {
    return this;
  }

  if(!this.query.filters) {
    this.query.filters = [];
  }

  this.query.filters.push({
    field : field,
    op : op,
    value : value
  });

  return this;
};

/* Orders query results by a field */
Query.prototype.orderBy = function(field, dir) {
  if(!this.query.orderBy) {
    this.query.orderBy = [];
  }

  this.query.orderBy.push({
    field : field,
    dir : dir
  });

  return this;
};

/* Runs the query, retrieving the first result */
Query.prototype.first = function(require) {
  return this.repo.first(this.context, require, this.query);
};

/* Runs the query */
Query.prototype.run = function() {
  return this.repo.query(this.context, this.query);
};


// Export the constructor

module.exports = {
  Query : Query
};
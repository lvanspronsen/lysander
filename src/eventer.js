var q = require('q');
var _ = require('underscore');

/* Event */

function Event() {
  this.listeners = [];
}

Event.prototype.addListener = function(listener) {
  this.listeners.push(listener);
};

Event.prototype.emit = function(args) {
  var proms = [];
  var me = this;

  _.each(me.listeners, function(listener) {
    proms.push(listener.apply(null, args));
  });

  return q.all(proms);
};

/* Eventer */

function Eventer() {
  this.events = {};  
}

Eventer.prototype.getEvent = function(name) {
  if(!this.events[name]) {
    this.events[name] = new Event();
  }
  return this.events[name];
}

Eventer.prototype.on = function(name, listener) {
  var ev = this.getEvent(name);
  ev.addListener(listener);
};

Eventer.prototype.emit = function(name) {
  var ev = this.getEvent(name);
  var newArgs = [];
  for(var i = 1; i < arguments.length; i++) {
    newArgs.push(arguments[i]);
  }
  return ev.emit(newArgs);
}

/* Exports */

module.exports = Eventer;
var _ = require('underscore');
var q = require('q');
var Repository = require('./repository.js');

function SoftAuditRepository(db) {
  Repository.apply(this, arguments);
  this.softDelete = false;
};
SoftAuditRepository.prototype = Object.create(Repository.prototype);

/* Retrieves the fields that are updateable */
SoftAuditRepository.prototype.getUpdateableFields = function(context) {
  var names = Repository.prototype.getUpdateableFields.apply(this, arguments);
  return _.without(names, 'created_at', 'updated_at', 'deleted_at');
};

/* Retrieves the fields that are insertable */
SoftAuditRepository.prototype.getInsertableFields = function(context) {
  var names = Repository.prototype.getInsertableFields.apply(this, arguments);
  return _.without(names, 'created_at', 'updated_at', 'deleted_at');
};

/* Finds an object by its id */
SoftAuditRepository.prototype.doFind = function(context, id, view) {
  if(!view) {
    view = this.table;
  }

  var query = this.db(view);
  if(context.transaction) {
    query.transacting(context.transaction);
  }

  if(this.softDelete) {
    query.whereNull('deleted_at');
  }

  this.defaultFilter(context,query,view);
  query.where('id', '=', id);

  return query.then(function(results) {
    if(results.length > 0) {
      return results[0];
    } else {
      throw new errors.NotFoundError('could not find object');
    }
  });
};

/* Performs an actual query on the database table or view */
SoftAuditRepository.prototype.doQuery = function(context, query) {
  var view = this.table;
  if(query.view && this.views.indexOf(query.view) !== -1) {
    view = query.view;
  }

  var q = this.db(view);
  if(context.transaction) {
    q.transacting(context.transaction);
  }

  if(this.softDelete) {
    q.whereNull('deleted_at');
  }

  /* apply additional clauses */
  this.defaultFilter(context, q, view);

  var queryableFields = this.getQueryableFields(context, view);
  this.applyFilters(q, queryableFields, query.filters || []);
  this.applyOrders(q, queryableFields, query.orderBy || []);
  this.applySkipAndTake(q, query.skip, query.take);

  if(query.count) {
    return q.count('*').then(function(result) {
      return { count : parseInt(result[0].aggregate) };
    })
  } else {
    this.applyFields(q, queryableFields, query.fields);
  }

  return q;
};

/* Performs an actual database insertion */
SoftAuditRepository.prototype.doInsert = function(context, obj) {
  var fields = this.getInsertableFields(context);
  obj = _.pick(obj, fields);

  obj.created_at = new Date();
  obj.updated_at = obj.created_at;
  obj.deleted_at = null;

  var q = this.db(this.table);
  if(context.transaction) {
    q.transacting(context.transaction);
  }

  return q.insert(obj, 'id')
    .then(function(newIds) { return newIds[0]; });
};


/* Performs an actual database update */
SoftAuditRepository.prototype.doUpdate = function(context, old, update) {
  var fields = this.getUpdateableFields(context);
  update = _.pick(update, fields);
  update.updated_at = new Date();

  var id = old.id;
  var q = this.db(this.table);
  if(context.transaction) {
    q.transacting(context.transaction);
  }

  return q.where('id', '=', id).update(update);
}

/* Performs the actual deletion of an object */
SoftAuditRepository.prototype.doDelete = function(context, id) {
  var update = { deleted_at : new Date() };
  var q = this.db(this.table);
  if(context.transaction) {
    q.transacting(context.transaction);
  }

  return q.where('id', '=', id).update(update);
};

module.exports = SoftAuditRepository;
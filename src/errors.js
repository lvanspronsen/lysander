var _ = require('underscore');
var q = require('q');

// Not Found Error

var NotFoundError = function(msg) {
  Error.apply(this, arguments);
  Error.captureStackTrace(this, this)
  this.message = msg;
};
NotFoundError.prototype = Object.create(Error.prototype);

// Access Denied Error

var AccessDeniedError = function(msg) {
  Error.apply(this, arguments);
  Error.captureStackTrace(this, this)
  this.message = msg;
};
AccessDeniedError.prototype = Object.create(Error.prototype);

// Invalid Credentials Error

var InvalidCredentialsError = function(msg) {
  Error.apply(this, arguments);
  Error.captureStackTrace(this, this)
  this.message = msg;
};
InvalidCredentialsError.prototype = Object.create(Error.prototype);

// Error Handler
var ErrorHandler = function() {
};

ErrorHandler.prototype.handle = function(res, err) {
  if(err instanceof Error) {
    res.send({ error : true, message : err.toString() }, 500);
  } else {
    res.send({ error : true, message : err }, 500);
  }
};

// export the constructor
module.exports = {
  NotFoundError : NotFoundError,
  AccessDeniedError : AccessDeniedError,
  ErrorHandler : ErrorHandler,
  InvalidCredentialsError : InvalidCredentialsError
};
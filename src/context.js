var _ = require('underscore');
var q = require('q');

// holds information regarding the context
// under which an operation is being executed.
// this includes the current user, the current http
// request and the current http response

function Context() {
  this.identity = null;
  this.request = null;
  this.response = null;
};

Context.prototype.getIdentity = function() { return this.identity; };

Context.prototype.setIdentity = function(identity) { this.identity = identity; };

Context.prototype.getRequest = function() { return this.request; };

Context.prototype.setRequest = function(request) { return this.request = request; }

Context.prototype.getResponse = function() { return this.response; }

Context.prototype.setResponse = function(response) { this.response = response; }

// context provider, which enables contexts
// on HTTP requests

var ContextProvider = function(identityProvider) {
  this.identityProvider = identityProvider;
};

ContextProvider.prototype.createContext = function() {
  return new Context();
};

ContextProvider.prototype.system = function() {
  var context = this.createContext();
  context.setIdentity(this.identityProvider.system());
  return context;
};

// Allows contexts to be attached to incoming HTTP requests
// via the req.context property
ContextProvider.prototype.host = function(app) {
  var me = this;

  app.use(function(req, res, next) {

    // create the context object
    var context = me.createContext();
    context.setIdentity(null);
    context.setRequest(req);
    context.setResponse(res);

    // we add the context object to the HTTP
    // request, so that we can retrieve it from
    // future middleware and handlers
    req.context = context;

    // proceed
    next();
  });
};

// export both constructors

module.exports = {
  Context : Context,
  ContextProvider : ContextProvider
};

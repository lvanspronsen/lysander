var _ = require('underscore');
var q = require('q');

var TableInfo = function(db, table) {
  this.db = db;
  this.table = table;
};

TableInfo.prototype.getColumns = function() {
  switch(this.db.client.dialect) {
    case 'postgresql':
      return this.getColumnsPostgres();
      break;
    case 'mysql':
      return this.getColumnsMySql();
      break;
    case 'sqlite':
      throw 'not implemented';
      break;
  }
};

TableInfo.prototype.getColumnsPostgres = function() {
  return this.db('information_schema.columns')
    .where('table_schema', '=', 'public')
    .where('table_name', '=', this.table)
    .select('column_name')
    .then(function(columns) {
      return _.map(columns, function(col) {
        return {
          name : col.column_name
        }
      });
    });
};

TableInfo.prototype.getColumnsMySql = function() {
  return this.db('information_schema.columns')
    .where('table_schema', '=', this.db.client.connectionSettings.database)
    .where('table_name', '=', this.table)
    .select('column_name')
    .then(function(columns) {
      return _.map(columns, function(col) {
        return {
          name : col.column_name
        }
      });
    });
};

// export the constructor
module.exports = TableInfo;